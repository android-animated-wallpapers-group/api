﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using AnimatedWallpapersApi.DAL;
using AnimatedWallpapersApi.Helpers;
using AnimatedWallpapersApi.Imgur;
using AnimatedWallpapersApi.Models;
using ImageProcessor;
using ImageProcessor.Imaging;
using ImageProcessor.Imaging.Formats;
using ImageProcessor.Processors;
using Imgur.API.Models;
using PagedList;

namespace AnimatedWallpapersApi.Controllers
{
    /// <summary>
    /// Wallpapers Controller description :)
    /// </summary>
    public class WallpapersController : ApiController
    {
        private WallpaperContext db = new WallpaperContext();

        // GET: api/Wallpapers
        /// <summary>
        /// Gets wallpapers using paging system.
        /// </summary>
        /// <param name="pageNo">Page number (default: 1)</param>
        /// <param name="pageSize">Page size (default: 50)</param>
        /// <param name="filter">Items filter: paid - paid wallpapers, free - free wallpapers, all - all wallpapers (default: all)</param>
        /// <param name="sort">Sorting by: id - id ascending, newest - id descending, top_downloads - downloads count descending (default: id)</param>
        /// <returns></returns>
        public IHttpActionResult GetWallpapers(int pageNo = 1, int pageSize = 50, string filter = "all",
            string sort = "id")
        {
            var skip = (pageNo - 1) * pageSize;

            IQueryable<Wallpaper> wallpapers;
            switch (filter)
            {
                case "paid":
                    wallpapers = from w in db.Wallpapers
                        where w.IsPaid
                        select w;
                    break;
                case "free":
                    wallpapers = from w in db.Wallpapers
                        where w.IsPaid == false
                        select w;
                    break;
                default:
                    wallpapers = from w in db.Wallpapers
                        select w;
                    break;
            }
            var total = wallpapers.Count();

            IQueryable<Wallpaper> sorted;
            switch (sort)
            {
                case "newest":
                    sorted = wallpapers.OrderByDescending(c => c.Id);
                    break;
                case "top_downloads":
                    sorted = wallpapers.OrderByDescending(c => c.DownloadCount);
                    break;
                default:
                    sorted = wallpapers.OrderBy(c => c.Id);
                    break;
            }

            // Select the customers based on paging parameters
            var res = sorted.Skip(skip)
                .Take(pageSize)
                .ToList();
            // Return the list of customers
            return Ok(new PagedResult<Wallpaper>(res, pageNo, pageSize, total));
        }

        // GET: api/Wallpapers/5
        /// <summary>
        /// Gets one wallpaper with full info. Inrcrements downloads count for wallpaper.
        /// </summary>
        /// <param name="id">Id of wallpaper</param>
        /// <returns></returns>
        [ResponseType(typeof(Wallpaper))]
        public async Task<IHttpActionResult> GetWallpaper(int id)
        {
            Wallpaper wallpaper;
            wallpaper = db.Wallpapers.Find(id);
            if (wallpaper == null)
            {
                return NotFound();
            }
            wallpaper.DownloadCount++;
            db.SaveChanges();

            return Ok(wallpaper);
        }

        /// <summary>
        /// Creates new wallpaper automatically on given url.
        /// </summary>
        /// <param name="url">Source url from web</param>
        /// <param name="name">Wallpaper name</param>
        /// <param name="description">Wallpaper description</param>
        /// <returns></returns>
        [ActionName("new")]
        [ResponseType(typeof(Wallpaper))]
        [HttpGet]
        public async Task<IHttpActionResult> NewWallpaper(string url, string name, string description)
        {
            Wallpaper wallpaper = await GenerateWallpaper(url, name, description);

            db.Wallpapers.Add(wallpaper);
            await db.SaveChangesAsync();

            return Ok(wallpaper);
        }

 
        /// <summary>
        /// Uploads a wallpaper and generates output transformations.
        /// </summary>
        /// <param name="name">Name</param>
        /// <param name="description">Description</param>
        /// <returns>Wallpaper item</returns>
        /// <exception cref="HttpResponseException">Exception</exception>
        [ActionName("post")]
        [ResponseType(typeof(Wallpaper))]
        [HttpPost]
        public async Task<HttpResponseMessage> PostFile(string name, string description)
        {
            // Check if the request contains multipart/form-data. 
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            string root = HttpContext.Current.Server.MapPath("~/App_Data");
            var provider = new MultipartFormDataStreamProvider(root);

            try
            {
                StringBuilder sb = new StringBuilder(); // Holds the response body 

                // Read the form data and return an async task. 
                await Request.Content.ReadAsMultipartAsync(provider);

                var file = provider.FileData.First();
           
                FileInfo fileInfo = new FileInfo(file.LocalFileName);
                Wallpaper wallpaper = await GenerateWallpaperUpload(file.LocalFileName, name, description);

                db.Wallpapers.Add(wallpaper);
                await db.SaveChangesAsync();

                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, wallpaper);

                return response;
            }
            catch (System.Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }

        private async Task<Wallpaper> GenerateWallpaper(string url, string name, string description)
        {
            Wallpaper wallpaper = new Wallpaper();
            ImgurHelper imgur = new ImgurHelper();
            IImage imgurImage = await imgur.UploadImageFromUrl(url);
            String link = imgurImage.Link;
            wallpaper.UrlFull = link;
            wallpaper.Name = name;
            wallpaper.Description = description;

            Uri uri = new Uri(wallpaper.UrlFull);
            String filename = uri.Segments[1]; //filename
            string fullSavePath = HttpContext.Current.Server.MapPath(string.Format("~/App_Data/{0}", filename));

            //DOWNLOADING
            using (var client = new WebClient())
            {
                client.DownloadFile(wallpaper.UrlFull, fullSavePath);
            }

            //CROP&RESIZE
            string procPath =
                Path.Combine(HttpContext.Current.Server.MapPath(string.Format("~/App_Data/{0}", "gifsicle.exe")));
            // --crop 306,0-1094,788 img2.gif > res.gif --optimize -O1 --resize 200x200
            int width = imgurImage.Width;
            int height = imgurImage.Height;
            Boolean cropWidth = width > height;
            int cropWindow;
            int x1, y1, x2, y2;
            if (cropWidth)
            {
                cropWindow = height;
                int cropLength = (width - height) / 2;
                x1 = cropLength;
                y1 = 0;
                x2 = cropLength + cropWindow;
                y2 = cropWindow;
            }
            else
            {
                cropWindow = width;
                int cropLength = (height - width) / 2;
                x1 = 0;
                y1 = cropLength;
                x2 = cropWindow;
                y2 = cropLength + cropWindow;
            }

            string cParams =
                $@"--batch --crop {x1},{y1}-{x2},{y2} {fullSavePath} --optimize -O3 --resize 200x200";
            //var proc = System.Diagnostics.Process.Start(procPath, cParams);
            //proc.WaitForExit(); //TODO max time
            StringBuilder sb = new StringBuilder();
            Process process = new Process();
            ProcessStartInfo StartInfo =
                new ProcessStartInfo()
                {
                    FileName = procPath,
                    Arguments = cParams,
                    UseShellExecute = false
                };
            process.StartInfo = StartInfo;
            process.Start();
            process.WaitForExit();

            byte[] photoBytes = File.ReadAllBytes(fullSavePath);

            //TRANSFORM TO JPG
            IImage gifImage = await imgur.UploadImageFromBinaryTable(photoBytes);
            wallpaper.UrlGif = gifImage.Link;

            ISupportedImageFormat format = new JpegFormat {Quality = 95};


            using (MemoryStream inStream = new MemoryStream(photoBytes))
            {
                using (MemoryStream outStream = new MemoryStream())
                {
                    // Initialize the ImageFactory using the overload to preserve EXIF metadata.
                    using (ImageFactory imageFactory = new ImageFactory(preserveExifData: false))
                    {
                        // Load, resize, set the format and quality and save an image.
                        imageFactory.Load(inStream)
                            .Format(format)
                            .Save(outStream);
                    }

                    IImage jpgImage = await imgur.UploadImageFromStream(outStream);
                    wallpaper.UrlStatic = jpgImage.Link;
                }
            }


            //byte[] photoBytes = File.ReadAllBytes(fullSavePath);
            //// Format is automatically detected though can be changed.
            //ISupportedImageFormat format = new JpegFormat {Quality = 95};
            //ISupportedImageFormat gifFormat = new GifFormat {Quality = 30};
            //Size size = new Size(200, 200);
            //ResizeLayer resize = new ResizeLayer(size, ResizeMode.Crop);
            //using (MemoryStream inStream = new MemoryStream(photoBytes))
            //{
            //    using (MemoryStream outStream = new MemoryStream())
            //    {
            //        using (MemoryStream jpgStream = new MemoryStream())
            //        {
            //            // Initialize the ImageFactory using the overload to preserve EXIF metadata.
            //            using (ImageFactory imageFactory = new ImageFactory(preserveExifData: false))
            //            {
            //                // Load, resize, set the format and quality and save an image.
            //                imageFactory.Load(inStream)
            //                    .Resize(resize)
            //                    .Format(gifFormat)
            //                    .Save(outStream);

            //                using (FileStream tempStream = File.Create(fullTempPath))
            //                {
            //                    await outStream.CopyToAsync(tempStream);
            //                }

            //                imageFactory.Load(outStream)
            //                    .Format(format)
            //                    .Save(jpgStream);
            //            }

            //            //RESIZE


            //            ImgurTask[] tasks = new ImgurTask[]
            //                {new ImgurTask(jpgStream, imgur), new ImgurTask(outStream, imgur)};
            //            await Task.WhenAll(tasks.Select(v => v.Run()));
            //            wallpaper.UrlStatic = tasks[0].ImageUrl;
            //            wallpaper.UrlGif = tasks[1].ImageUrl;
            //        }
            //    }
            //}
            File.Delete(fullSavePath);
            return wallpaper;
        }

        private async Task<Wallpaper> GenerateWallpaperUpload(string localFile, string name, string description)
        {
            Wallpaper wallpaper = new Wallpaper();
            ImgurHelper imgur = new ImgurHelper();

            byte[] photoBytesOriginal = File.ReadAllBytes(localFile);
            IImage imgurImage = await imgur.UploadImageFromBinaryTable(photoBytesOriginal);

            //TRANSFORM TO JPG
            String link = imgurImage.Link;
            wallpaper.UrlFull = link;
            wallpaper.Name = name;
            wallpaper.Description = description;


            string fullSavePath = localFile;


            //CROP&RESIZE
            string procPath =
                Path.Combine(HttpContext.Current.Server.MapPath(string.Format("~/App_Data/{0}", "gifsicle.exe")));
            // --crop 306,0-1094,788 img2.gif > res.gif --optimize -O1 --resize 200x200
            int width = imgurImage.Width;
            int height = imgurImage.Height;
            Boolean cropWidth = width > height;
            int cropWindow;
            int x1, y1, x2, y2;
            if (cropWidth)
            {
                cropWindow = height;
                int cropLength = (width - height) / 2;
                x1 = cropLength;
                y1 = 0;
                x2 = cropLength + cropWindow;
                y2 = cropWindow;
            }
            else
            {
                cropWindow = width;
                int cropLength = (height - width) / 2;
                x1 = 0;
                y1 = cropLength;
                x2 = cropWindow;
                y2 = cropLength + cropWindow;
            }

            string cParams =
                $@"--batch --crop {x1},{y1}-{x2},{y2} {fullSavePath} --optimize -O3 --resize 200x200";
            //var proc = System.Diagnostics.Process.Start(procPath, cParams);
            //proc.WaitForExit(); //TODO max time
            StringBuilder sb = new StringBuilder();
            Process process = new Process();
            ProcessStartInfo StartInfo =
                new ProcessStartInfo()
                {
                    FileName = procPath,
                    Arguments = cParams,
                    UseShellExecute = false
                };
            process.StartInfo = StartInfo;
            process.Start();
            process.WaitForExit();

            byte[] photoBytes = File.ReadAllBytes(fullSavePath);

            //TRANSFORM TO JPG
            IImage gifImage = await imgur.UploadImageFromBinaryTable(photoBytes);
            wallpaper.UrlGif = gifImage.Link;

            ISupportedImageFormat format = new JpegFormat {Quality = 95};


            using (MemoryStream inStream = new MemoryStream(photoBytes))
            {
                using (MemoryStream outStream = new MemoryStream())
                {
                    // Initialize the ImageFactory using the overload to preserve EXIF metadata.
                    using (ImageFactory imageFactory = new ImageFactory(preserveExifData: false))
                    {
                        // Load, resize, set the format and quality and save an image.
                        imageFactory.Load(inStream)
                            .Format(format)
                            .Save(outStream);
                    }

                    IImage jpgImage = await imgur.UploadImageFromStream(outStream);
                    wallpaper.UrlStatic = jpgImage.Link;
                }
            }


            //byte[] photoBytes = File.ReadAllBytes(fullSavePath);
            //// Format is automatically detected though can be changed.
            //ISupportedImageFormat format = new JpegFormat {Quality = 95};
            //ISupportedImageFormat gifFormat = new GifFormat {Quality = 30};
            //Size size = new Size(200, 200);
            //ResizeLayer resize = new ResizeLayer(size, ResizeMode.Crop);
            //using (MemoryStream inStream = new MemoryStream(photoBytes))
            //{
            //    using (MemoryStream outStream = new MemoryStream())
            //    {
            //        using (MemoryStream jpgStream = new MemoryStream())
            //        {
            //            // Initialize the ImageFactory using the overload to preserve EXIF metadata.
            //            using (ImageFactory imageFactory = new ImageFactory(preserveExifData: false))
            //            {
            //                // Load, resize, set the format and quality and save an image.
            //                imageFactory.Load(inStream)
            //                    .Resize(resize)
            //                    .Format(gifFormat)
            //                    .Save(outStream);

            //                using (FileStream tempStream = File.Create(fullTempPath))
            //                {
            //                    await outStream.CopyToAsync(tempStream);
            //                }

            //                imageFactory.Load(outStream)
            //                    .Format(format)
            //                    .Save(jpgStream);
            //            }

            //            //RESIZE


            //            ImgurTask[] tasks = new ImgurTask[]
            //                {new ImgurTask(jpgStream, imgur), new ImgurTask(outStream, imgur)};
            //            await Task.WhenAll(tasks.Select(v => v.Run()));
            //            wallpaper.UrlStatic = tasks[0].ImageUrl;
            //            wallpaper.UrlGif = tasks[1].ImageUrl;
            //        }
            //    }
            //}
            File.Delete(fullSavePath);
            return wallpaper;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}