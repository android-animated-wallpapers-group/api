﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AnimatedWallpapersApi.Models
{
    public class Wallpaper
    {
        public bool IsPaid { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string UrlGif { get; set; }
        public string UrlStatic { get; set; }
        public int DownloadCount { get; set; }
        public int Id { get; set; }
        public string UrlFull { get; set; }
    }
}