﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AnimatedWallpapersApi.Models
{
    public class Application
    {
        public int ApplicationId { get; set; }
        public string Name { get; set; }
        public string Owner { get; set; }
        public string Email { get; set; }
        public int ApiCalls { get; set; }
        public string SecretKey { get; set; }
    }
}