﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Policy;
using System.Threading.Tasks;
using System.Web;
using Imgur.API;
using Imgur.API.Authentication.Impl;
using Imgur.API.Endpoints.Impl;
using Imgur.API.Models;

namespace AnimatedWallpapersApi.Imgur
{
    public class ImgurHelper
    {
        private const string ClientId = "1658c77a6ab8a3a";
        private const string ClientSecret = "1c56f9f36542912eec39ba037f28a074c7d770b4";

        public async Task<IImage> UploadImageFromUrl(string imageUrl)
        {
            if (imageUrl == null) throw new ArgumentNullException(nameof(imageUrl));
            try
            {
                var client = new ImgurClient(ClientId, ClientSecret);
                var endpoint = new ImageEndpoint(client);
                Uri url = new Uri(imageUrl);
                string path = url.AbsoluteUri;
                var image = await endpoint.UploadImageUrlAsync(path);
                return image;
            }
            catch (ImgurException imgurEx)
            {
                throw new Exception("An error occurred uploading an image to Imgur: " +imgurEx.Message);
            }
        }

        public async Task<IImage> UploadImageFromStream(Stream imageStream)
        {
            if (imageStream == null) throw new ArgumentNullException(nameof(imageStream));
            try
            {
                var client = new ImgurClient(ClientId, ClientSecret);
                var endpoint = new ImageEndpoint(client);
                var image = await endpoint.UploadImageStreamAsync(imageStream);
                return image;
            }
            catch (ImgurException imgurEx)
            {
                throw new Exception("An error occurred uploading an image to Imgur: " + imgurEx.Message);
            }
        }

        public async Task<IImage> UploadImageFromBinaryTable(byte[] imageStream)
        {
            if (imageStream == null) throw new ArgumentNullException(nameof(imageStream));
            try
            {
                var client = new ImgurClient(ClientId, ClientSecret);
                var endpoint = new ImageEndpoint(client);
                var image = await endpoint.UploadImageBinaryAsync(imageStream);
                return image;
            }
            catch (ImgurException imgurEx)
            {
                throw new Exception("An error occurred uploading an image to Imgur: " + imgurEx.Message);
            }
        }
    }
}