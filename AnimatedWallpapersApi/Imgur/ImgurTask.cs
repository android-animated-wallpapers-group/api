﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Imgur.API.Models;

namespace AnimatedWallpapersApi.Imgur
{
    public class ImgurTask
    {
        private MemoryStream stream;
        private ImgurHelper helper;
        private string url;

        public ImgurTask(MemoryStream stream, ImgurHelper helper)
        {
            this.stream = stream;
            this.helper = helper;
        }

        public async Task Run()
        {
            if(stream==null || helper==null) throw new NullReferenceException("Stream or ImgurHelper is null.");
            IImage image = await helper.UploadImageFromStream(stream);
            url = image.Link;
        }

        public string ImageUrl => url;

    }
}