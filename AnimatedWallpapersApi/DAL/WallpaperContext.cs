﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;
using AnimatedWallpapersApi.Models;

namespace AnimatedWallpapersApi.DAL
{
    public class WallpaperContext : DbContext
    {
        public WallpaperContext() : base("WallpaperContext")
        {
        }

        public DbSet<Application> Applications { get; set; }
        public DbSet<Wallpaper> Wallpapers { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}