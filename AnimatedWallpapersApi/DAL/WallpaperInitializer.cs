﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AnimatedWallpapersApi.Models;

namespace AnimatedWallpapersApi.DAL
{
    public class WallpaperInitializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<WallpaperContext>
    {
        protected override void Seed(WallpaperContext context)
        {
            var freeWallpapers = new List<Wallpaper>
            {
                new Wallpaper
                {
                    Name = "Stripes",
                    Description = "Animated stripe",
                    IsPaid = false,
                    UrlFull =
                        "http://www.techagesite.com/Animated%20phone%20wallpapers/hd-free-download-gif-wallpaper-for-mobile.gif",
                    UrlGif = "http://i.imgur.com/SYT16Tr.gif",
                    UrlStatic = "http://i.imgur.com/MdZtnaB.gif"
                },
                new Wallpaper
                {
                    Name = "Squares",
                    Description = "Squares changing color",
                    IsPaid = false,
                    UrlFull =
                        "https://s-media-cache-ak0.pinimg.com/originals/a6/f3/8a/a6f38a04479024e374520766830f88ea.gif",
                    UrlGif = "http://i.imgur.com/XZ73dlL.gif",
                    UrlStatic = "http://i.imgur.com/cYGE7zz.gif"
                },
                new Wallpaper
                {
                    Name = "Rocks",
                    Description = "Water with rocks",
                    IsPaid = true,
                    UrlFull =
                        "http://www.techagesite.com/Animated%20phone%20wallpapers/animated-mobile-phone-wallpapers-gif-beautiful-rock-pool.gif",
                    UrlGif = "http://i.imgur.com/V7fKtjW.gif",
                    UrlStatic = "http://i.imgur.com/IDJDIC6.gif"
                },
                new Wallpaper
                {
                    Name = "Nature",
                    Description = "Nature by night",
                    IsPaid = true,
                    UrlFull = "http://www.goodlightscraps.com/content/nature/nature-images-8.gif",
                    UrlGif = "http://www.goodlightscraps.com/content/nature/nature-images-8.gif",
                    UrlStatic = "http://i.imgur.com/fSWrTVN.gif"
                },
                new Wallpaper
                {
                    Name = "Waterfall",
                    Description = "Waterfall amongst rocks",
                    IsPaid = false,
                    UrlFull =
                        "http://bestanimations.com/Nature/beautiful-forest-waterfall-rocks-nature-animated-gif.gif",
                    UrlGif = "http://i.imgur.com/pZjTuvN.gif",
                    UrlStatic = "http://i.imgur.com/r9EEFDK.gif"
                },
                new Wallpaper
                {
                    Name = "WUT",
                    Description = "Warsaw University of Technology",
                    IsPaid = true,
                    UrlFull = "http://i.imgur.com/dcuj0Ob.gif",
                    UrlGif = "http://i.imgur.com/kclE1nZ.gif",
                    UrlStatic = "http://i.imgur.com/484OD5T.gif"
                }
            };
            freeWallpapers.ForEach(v => context.Wallpapers.Add(v));
            context.SaveChanges();

        }
    }
}